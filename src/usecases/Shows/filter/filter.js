export default function filter(shows) {
  const filteredShows = shows.filter(
    (show) => show.drm === true && show.episodeCount > 0
  );

  return filteredShows.map((show) => {
    return {
      image: { showImage: show.image.showImage },
      slug: show.slug,
      title: show.title,
    }
  })
};
