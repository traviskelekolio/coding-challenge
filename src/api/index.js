import { Router } from 'express';
import shows from './shows';

const api = Router();

api.use('/shows', shows);

export default api;
