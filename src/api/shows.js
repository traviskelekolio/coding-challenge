import { Router } from 'express';
import { ShowsController } from '../controllers';

const shows = Router();

shows
  .route('/filter')
  .post(ShowsController.filter);

export default shows;
