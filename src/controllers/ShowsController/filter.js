import useCase from '../../usecases/Shows/filter/filter.js'

export default function filter(req, res) {
    const shows = req.body.payload;
    const response = useCase(shows);

    res.json({ status: 200, message: "success", response })
}
