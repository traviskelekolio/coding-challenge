import filter from './filter';

const actions = {
  filter,
};

export default actions;
