import api from './api'

const express = require('express');
const app = express();
const PORT = 3000;

app.use(express.json())
app.use((err, req, res, next) => {
    if (err instanceof SyntaxError && err.status === 400 && 'body' in err) {
        return res.json({ status: 400, error: "Could not decode request: JSON parsing failed"});
    }
    next();
});
app.use('/nine', api);

app.listen(PORT, function() {
    console.log('Server is running on PORT:', PORT);
});
